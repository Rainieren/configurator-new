const colors = require('tailwindcss/colors');

module.exports = {
    darkMode: 'class',
    important: true,
    purge: [
      './resources/**/*.blade.php',
      './resources/**/*.js',
      './resources/**/*.vue',
    ],
    theme: {
      extend: {
          fontWeight: ['hover']
      },
      colors: {
        bluegray: colors.blueGray,
        coolgray: colors.coolGray,
        gray: colors.gray,
        truegray: colors.trueGray,
        warmgray: colors.warmGray,
        red: colors.red,
        orange: colors.orange,
        amber: colors.amber,
        yellow: colors.yellow,
        lime: colors.lime,
        green: colors.green,
        emerald: colors.emerald,
        teal: colors.teal,
        cyan: colors.cyan,
        sky: colors.sky,
        blue: colors.blue,
        indigo: colors.indigo,
        violet: colors.violet,
        purple: colors.purple,
        fuchsia: colors.fuchsia,
        pink: colors.pink,
        rose: colors.rose,
        black: colors.black,
        white: colors.white
      },
    },
    variants: {
    extend: {},
    },
    plugins: [
      require('@tailwindcss/forms')({
          strategy: 'class',
      }),
      require('@tailwindcss/typography')
    ],
}
