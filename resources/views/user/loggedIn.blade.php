<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Mountain-iT Product Configurator</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
</head>
<body class="antialiased">

<div class="container">
    <lottie-player src="https://assets7.lottiefiles.com/packages/lf20_01bh9ld2.json"  background="transparent"  speed="1"  style="max-width: 500px; max-height: 500px; margin: 0 auto" loop autoplay></lottie-player>

    <div style="max-width: 500px; max-height: 500px; margin: 0 auto; text-align: center;">
        <h1 class="text-2xl font-bold">You are logged in!</h1>
        <h2 class="text-l">Please continue at the original tab.</h2>
    </div>

</div>

</body>

</html>
