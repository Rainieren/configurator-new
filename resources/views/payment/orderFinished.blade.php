<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Mountain-iT Product Configurator</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
</head>
<body class="antialiased">

<div class="container">
    <lottie-player src="https://assets8.lottiefiles.com/packages/lf20_3WsNKy.json"  background="transparent"  speed="1"  style="width: 500px; height: 500px; margin: 0 auto;"  loop autoplay></lottie-player>

    <div style="max-width: 500px; max-height: 500px; margin: 0 auto; text-align: center;">
        <h1 class="text-2xl font-bold">Thanks for your purchase.</h1>
        <h2 class="text-l">You're order is being processed.
            <br/>
            All updates will be sent to your email.</h2>
        <a href="/"><button type="button" class="bg-transparent hover:bg-indigo-500 text-indigo-700 font-semibold hover:text-white py-2 px-4 border border-indigo-500 hover:border-transparent rounded mt-5">Return to Homepage</button></a>
    </div>

</div>

</body>

</html>
